'use strict';

/*
    This file can be used as entry point for webpack!
 */

import './plugins/just-validate-custom/just-validate-custom.js';
import 'components/ui/ui.js';
import 'components/product/product.js';
import 'components/contacts/contacts.js';
import 'components/product/product-detail.js';
import 'components/brands/brands.js';
import 'components/user/user.js';
import 'components/cart/cart.js';
