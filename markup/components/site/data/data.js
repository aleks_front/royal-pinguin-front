let data = {
    site: {
        common: {
            name: 'Royal Pinguin',
            slogan: '',
            logo: 'logo.svg'
        },
        pages: {
            defaults: {
                title: 'Page title',
                description: 'Description page',
                useSocialMetaTags: false,
                name: 'Defaults',
                value: 'defaults'
            },
            primary: {
                news: {
                    title: 'Новости',
                    description: 'Новости',
                    useSocialMetaTags: true,
                    name: 'Новости',
                    value: 'news'
                }
            },
            secondary: {
                defaults: {
                    title: 'Page title',
                    description: 'Description page',
                    useSocialMetaTags: false,
                    name: 'Defaults',
                    value: 'defaults'
                },
                payment: {
                    title: 'Оплата',
                    description: 'Оплата',
                    useSocialMetaTags: true,
                    name: 'Оплата',
                    value: 'payment'
                },
                contacts: {
                    title: 'Наши магазины',
                    description: 'Наши магазины',
                    useSocialMetaTags: true,
                    name: 'Наши магазины',
                    value: 'contacts'
                }
            },
            user: {
                defaults: {
                    title: 'Page title',
                    description: 'Description page',
                    useSocialMetaTags: false,
                    name: 'Defaults',
                    value: 'defaults'
                },
                login: {
                    title: 'Войти',
                    description: 'Личный кабинет',
                    useSocialMetaTags: false,
                    name: 'Войти',
                    value: 'login'
                }
            },
            help: {
                defaults: {
                    title: 'Page title',
                    description: 'Description page',
                    useSocialMetaTags: false,
                    name: 'Defaults',
                    value: 'defaults'
                },
                delivery: {
                    title: 'Доставка',
                    description: 'Доставка',
                    useSocialMetaTags: false,
                    name: 'Доставка',
                    value: 'delivery'
                },
                payment: {
                    title: 'Оплата',
                    description: 'Оплата',
                    useSocialMetaTags: false,
                    name: 'Оплата',
                    value: 'payment'
                },
                ourStores: {
                    title: 'Наши',
                    description: 'Доставка',
                    useSocialMetaTags: false,
                    name: 'Доставка',
                    value: 'our-stores'
                }
            },
            cart: {
                defaults: {
                    title: 'Page title',
                    description: 'Description page',
                    useSocialMetaTags: false,
                    name: 'Defaults',
                    value: 'defaults'
                },
                cart: {
                    title: 'Ваша корзина',
                    description: 'Ваша корзина',
                    useSocialMetaTags: false,
                    name: 'Ваша корзина',
                    value: 'cart'
                }
            }
        }
    }
};
