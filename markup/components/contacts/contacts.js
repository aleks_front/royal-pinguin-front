import { closest } from '../ui/js/_util';

/* global ymaps pinUrl */
const latlang = (data) => {
    return data.split(',').map((n) => {
        return Number(n);
    });
};

let contacts = false;
const contactsMap = document.querySelector('.contacts__map');
let map;
let markers = [];
let routeButtonControl;
let route;
let remove;


// user position
// const userPosition = () => {
//     let position = [];
//     return ymaps.geolocation.get({
//         provider: 'browser',
//         mapStateAutoApply: true
//     }).then(function (result) {
//         position = result;
//         return position;
//     }, function (error) {
//         console.log(error);
//     }, this);
// };
// remove route
const removeRoute = () => {
    if (!remove) {
        remove = new ymaps.control.Button({
            data: {
                content: 'Удалить маршрут',
                title: 'Нажмите удаления маршрута'
            },
            options: {
                selectOnClick: false,
                maxWidth: 200
            }
        });
        map.controls.add(remove, { float: 'left', floatIndex: 100 });

        remove.events.add('click', () => {
            map.geoObjects.remove(route);
            route = [];
            map.controls.remove('routeButtonControl');
            routeButtonControl = false;
            map.controls.remove(remove);
            remove = false;

            // set cennter for map
            map.setBounds(map.geoObjects.getBounds(), {
                checkZoomRange: true,
            });
        });
    }
};

// route panel
const drawRoute = (point) => {
    contactsMap.classList.add('is-loading');
    if (!routeButtonControl) {
        map.controls.add('routeButtonControl', {
            size: 'small',
            float: 'left',
            floatIndex: 1000,
        });
        routeButtonControl = map.controls.get('routeButtonControl');
    }
    routeButtonControl.routePanel.options.set('allowSwitch', false);
    routeButtonControl.routePanel.state.set({
        toEnabled: false,
        to: point,
        type: 'auto'
    });

    routeButtonControl.routePanel.getRouteAsync().then((multiRoute) => {
        removeRoute();
        route = multiRoute;
        multiRoute.events.add('update', () => {
            contactsMap.classList.remove('is-loading');
            multiRoute.options.set({
                wayPointFinishVisible: false,
            });
        });
    });

    let from = routeButtonControl.routePanel.state.get('from') ? true : false;
    ymaps.geolocation.get({
        provider: 'browser',
        mapStateAutoApply: true
    }).then(function (result) {
        from = result.geoObjects.position;
        routeButtonControl.routePanel.state.set('from', from);
        contactsMap.classList.remove('is-loading');
        routeButtonControl.state.set('expanded', true);
    }, function (error) {
        contactsMap.classList.remove('is-loading');
        if (!from) {
            contactsMap.classList.add('is-message');
        }
    }, this);
};

// add custom pin for markerplace
const addMarker = (data) => {
    let placemark = new ymaps.Placemark(data, null, {
        iconLayout: 'default#image',
        iconImageHref: '/wp-content/themes/royalpinguin/static/img/general/pin.svg',
        iconImageSize: [65, 65],
        iconImageOffset: [-32, -32]
    });

    // add route between curent user position to clicked
    placemark.events.add('click', () => {
        // placemark.options.set('visible', false);
        drawRoute(data);
    });

    if ( markers ) {
        markers.push(placemark);
    }

    map.geoObjects.add(placemark);
};

// draw points on the map
const drawPoint = (container) => {
    let points;
    if (container) {
        points = container.querySelectorAll('[data-toggle="map"]');
    } else {
        points = document.querySelectorAll('[data-toggle="map"]');
    }
    if (points.length && map) {
        // set markers
        [...points].forEach((point, index) => {
            point = point.getAttribute('data-address') ? point.getAttribute('data-address') : false;
            if (point) {
                point = point.split(',').map((n) => {
                    return Number(n);
                });
                // add markers
                addMarker(point);
            }
        });

        // set cennter for map
        map.setBounds(map.geoObjects.getBounds(), {
            checkZoomRange: true,
        });
    }
};

// remove poinst on the map
const removePoint = (points) => {
    console.log('removePoint');
    if (points.length && map) {
        [...points].forEach((marker) => {
            map.geoObjects.remove(marker);
        });
        markers = [];
        return markers;
    }
};

// remove all overlay map
const removeAll = () => {
    removePoint(markers);
    map.controls.remove('routeButtonControl');
    map.geoObjects.remove(route);
    route = [];
    map.controls.remove('routeButtonControl');
    routeButtonControl = false;
    map.controls.remove(remove);
    remove = false;
};

document.addEventListener('click', (e) => {
    const element = e.target;
    const dismiss = element.getAttribute('data-dismiss');
    if (element && dismiss === 'message') {
        contactsMap.classList.remove('is-message');
        if (routeButtonControl) {
            routeButtonControl.state.set('expanded', true);
        }
    }
});

// init map
const init = () => {
    map = new ymaps.Map(contactsMap, {
        center: [55.753239, 37.622552],
        zoom: 16,
        controls: []
    });

    // zoom control
    map.controls.add('zoomControl', {
        size: 'small',
        position: {
            right: '5px',
            bottom: '30px'
        }
    });

    // disable zoom on scroll
    map.behaviors.disable('scrollZoom');

    // fullscreen control
    map.controls.add('fullscreenControl', {
        size: 'small',
        float: 'right'
    });
    contactsMap.classList.remove('is-loading');
    document.addEventListener('click', (e) => {
        let el = e.target;
        if (el.classList.contains('contacts__route')) {
            e.preventDefault();
            let point = closest(el, '[data-toggle="map"]');
            if (point) {
                let target = point.getAttribute('data-address');
                target = latlang(target);
                drawRoute(target);
            }
        }
    });

    // contacts width tabs
    contacts = document.querySelector('.contacts--map') ? document.querySelector('.contacts--map') : document.querySelector('.contacts--detail');
    if (contacts.classList.contains('contacts--map')) {
        let contactsTabs = contacts.querySelectorAll('.tabs__body');
        drawPoint(contactsTabs[0]);
        [...contactsTabs].forEach( (tab) => {
            tab.addEventListener('tabs.show', (e) => {
                removeAll();
                drawPoint(tab);
            });
        });
    } else if (contacts.classList.contains('contacts--detail')) {
        drawPoint(contacts);
    }
};

if (contactsMap) {
    ymaps.ready(init);
}
