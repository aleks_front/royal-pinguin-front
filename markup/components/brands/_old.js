
import {addListenerMultiple, breakpoints, getOffset, forEach} from '../ui/js/_util';
import Swiper from 'swiper';

let brands = document.querySelector('#brands');
let sliderContainer = brands.querySelector('.brands__container');
let sliderWrapper = brands.querySelector('.brands__row');
let sliderSlide = brands.querySelectorAll('.brands__col');
let sliderBtnPrev = brands.querySelector('.brands__btn--prev');
let sliderBtnNext = brands.querySelector('.brands__btn--next');
let sliderParam = {
    slidesPerView: 5,
    spaceBetween: 20,
    navigation: {
        nextEl: sliderBtnNext,
        prevEl: sliderBtnPrev
    },
    breakpoints: {
        992: {
            slidesPerView: 4,
            spaceBetween: 20
        }
    }
};
let slider;

const init = function () {
    sliderContainer.classList.add('swiper-container');
    sliderWrapper.classList.add('swiper-wrapper');
    [...sliderSlide].forEach((element, index) => {
        element.classList.add('swiper-slide');
    });
    slider = new Swiper(sliderContainer, sliderParam);
};

const destroy = function () {
    slider.destroy(true, true);
    sliderContainer.classList.remove('swiper-container');
    sliderWrapper.classList.remove('swiper-wrapper');
    [...sliderSlide].forEach((element, index) => {
        element.classList.remove('swiper-slide');
    });
};

let resizeTimer;
const resize = function () {
    clearTimeout(resizeTimer);
    resizeTimer = setTimeout(function () {
        if (getOffset(document.documentElement).width >= breakpoints.md) {
            init();
        } else if (slider && slider.initialized) {
            destroy();
        }
    }, 250);
};

addListenerMultiple(window, 'load resize', resize);

