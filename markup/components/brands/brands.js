
import {addListenerMultiple, breakpoints, getOffset} from '../ui/js/_util';
import Swiper from 'swiper';


const init = function (data) {
    data.block.classList.add('brands--grid-slider');
    data.block.classList.remove('brands--grid');
    data.container.classList.add('swiper-container');
    data.wrapper.classList.add('swiper-wrapper');
    [...data.slide].forEach((element) => {
        element.classList.add('swiper-slide');
    });
    data.object = new Swiper(data.container, {
        slidesPerView: 5,
        spaceBetween: 20,
        navigation: {
            nextEl: data.btnNext,
            prevEl: data.btnPrev
        },
        breakpoints: {
            992: {
                slidesPerView: 4,
                spaceBetween: 20
            }
        }
    });
};

const destroy = function (data) {
    data.object.destroy(true, true);
    data.block.classList.add('brands--grid');
    data.block.classList.remove('brands--grid-slider');
    data.container.classList.remove('swiper-container');
    data.wrapper.classList.remove('swiper-wrapper');
    [...data.slide].forEach((element, index) => {
        element.classList.remove('swiper-slide');
    });
};

let resizeTimer;
const resize = function (data) {
    clearTimeout(resizeTimer);
    resizeTimer = setTimeout(function () {
        if (getOffset(document.documentElement).width >= breakpoints.md) {
            init(data);
        } else if (data.object && data.object.initialized) {
            destroy(data);
        }
    }, 250);
};

let brands = document.querySelectorAll('.brandsLogoJs');

if (brands) {
    [...brands].forEach((element, index) => {
        let slider = [];
        slider.object = '';
        slider.block = element;
        slider.container = element.querySelector('.brands__container');
        slider.wrapper = element.querySelector('.brands__row');
        slider.slide = element.querySelectorAll('.brands__col');
        slider.btnPrev = element.querySelector('.brands__btn--prev');
        slider.btnNext = element.querySelector('.brands__btn--next');

        addListenerMultiple(window, 'load resize', function () {
            resizeTimer = '';
            resize(slider);
        });
    });
}
