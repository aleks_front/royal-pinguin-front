var data = {
    brands: {
        logo: [
            {
                title: 'Название бренда',
                img: {
                    x1: 'brand__img-1.png',
                    x2: 'brand__img-1@2x.png'
                }
            },
            {
                title: 'Название бренда',
                img: {
                    x1: 'brand__img-2.png',
                    x2: 'brand__img-2@2x.png'
                }
            },
            {
                title: 'Название бренда',
                img: {
                    x1: 'brand__img-3.png',
                    x2: 'brand__img-3@2x.png'
                }
            },
            {
                title: 'Название бренда',
                img: {
                    x1: 'brand__img-4.png',
                    x2: 'brand__img-4@2x.png'
                }
            },
            {
                title: 'Название бренда',
                img: {
                    x1: 'brand__img-5.png',
                    x2: 'brand__img-5@2x.png'
                }
            },
            {
                title: 'Название бренда',
                img: {
                    x1: 'brand__img-1.png',
                    x2: 'brand__img-1@2x.png'
                }
            },
            {
                title: 'Название бренда',
                img: {
                    x1: 'brand__img-2.png',
                    x2: 'brand__img-2@2x.png'
                }
            },
            {
                title: 'Название бренда',
                img: {
                    x1: 'brand__img-3.png',
                    x2: 'brand__img-3@2x.png'
                }
            },
            {
                title: 'Название бренда',
                img: {
                    x1: 'brand__img-4.png',
                    x2: 'brand__img-4@2x.png'
                }
            },
            {
                title: 'Название бренда',
                img: {
                    x1: 'brand__img-5.png',
                    x2: 'brand__img-5@2x.png'
                }
            }
        ]
    }
};
