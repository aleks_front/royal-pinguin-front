'use strict';
import {addListenerMultiple, breakpoints, getOffset} from '../ui/js/_util';

const show = (toggle, target) => {
    target.style.display = 'block';
    setTimeout( () => {
        target.classList.add('is-open');
        toggle.classList.add('is-active');
    }, 250);
};

const hide = (toggle, target) => {
    toggle.classList.remove('is-active');
    target.classList.remove('is-open');
    setTimeout( () => {
        target.style.display = 'none';
    }, 250);
};

document.addEventListener('click', (e) => {
    let el = e.target;
    let toggle = el.getAttribute('data-toggle') === 'cart' ? el : false;
    let target = el.getAttribute('data-target') ? document.querySelector(el.getAttribute('data-target')) : false;
    if (toggle && target) {
        e.preventDefault();
        if (getOffset(document.documentElement).width >= breakpoints.lg) {
            if (target.classList.contains('is-open')) {
                hide(toggle, target);
            } else {
                show(toggle, target);
            }
        }
    }
});

let resizeTimer;
const resize = () => {
    clearTimeout(resizeTimer);
    resizeTimer = setTimeout(() => {
        if (getOffset(document.documentElement).width < breakpoints.lg) {
            let carts = document.querySelectorAll('[data-toggle="cart"]');
            [...carts].forEach((el) => {
                let target = el.getAttribute('data-target') ? document.querySelector(el.getAttribute('data-target')) : false;
                if (target) {
                    hide(el, target);
                }
            });
        }
    }, 250);
};

addListenerMultiple(window, 'load resize', function () {
    resizeTimer = '';
    resize();
});
