import noUiSlider from 'noUiSlider';
import wNumb from 'wNumb';

const priceFormat =  wNumb({
    decimals: 0,
    thousand: ' '
}); 

let range = [];
range.element = document.querySelector('.rangeJs');

if( range && range.element ) {

    range.slider = document.querySelector('.form__slider');
    range.from = range.element.querySelector('.form__control--from');
    range.from.hid = range.from.nextElementSibling ? range.from.nextElementSibling : false;

    range.to = range.element.querySelector('.form__control--to');
    range.to.hid = range.to.nextElementSibling ? range.to.nextElementSibling : false;

    range.min = range.from.min ? priceFormat.from(range.from.min) : 0;
    range.max = range.to.max ? priceFormat.from(range.to.max) : 1000000;
    range.control = [range.from, range.to];
    range.control.hid = [range.from.hid, range.to.hid];


    const setSliderHandle = function (i, value) {
        var r = [null,null];
        r[i] = value;
        range.slider.noUiSlider.set(r);
    };

    const syncInputHandle = function (i, value) {
        range.control.hid[i].value = priceFormat.from(value);
    };

    range.object = noUiSlider.create(range.slider, {
        start: [range.from.value, range.to.value],
        connect: true,
        range: {
            'min': range.min,
            'max': range.max
        },
        format: priceFormat
    });
    range.slider.noUiSlider.on('update', function( values, handle ) {
        range.control[handle].value = values[handle];
        syncInputHandle(handle, values[handle]);
    });

    // Listen to keydown events on the input field.
    
    [...range.control].forEach((input, handle) => {
        input.addEventListener('change', function(){
            let value = this.value;
            setSliderHandle(handle, value);
            syncInputHandle(handle, value);
        });

        input.addEventListener('keydown', function( e ) {
    
            let values = range.slider.noUiSlider.get();
            let value = Number(values[handle]);
    
            // [[handle0_down, handle0_up], [handle1_down, handle1_up]]
            let steps = range.slider.noUiSlider.steps();
    
            // [down, up]
            let step = steps[handle];
    
            let position;
    
            // 13 is enter,
            // 38 is key up,
            // 40 is key down.
            switch ( e.which ) {
    
                case 13:
                    setSliderHandle(handle, this.value);
                    syncInputHandle(handle, this.value);
                    break;
    
                case 38:
    
                    // Get step to go increase slider value (up)
                    position = step[1];
    
                    // false = no step is set
                    if ( position === false ) {
                        position = 1;
                    }
    
                    // null = edge of slider
                    if ( position !== null ) {
                        setSliderHandle(handle, value + position);
                    }
    
                    break;
    
                case 40:
    
                    position = step[0];
    
                    if ( position === false ) {
                        position = 1;
                    }
    
                    if ( position !== null ) {
                        setSliderHandle(handle, value - position);
                    }
    
                    break;
            }
        });

    });
}
