import {addListenerMultiple, closest, forEach} from './_util';

let placeholders = document.querySelectorAll('[data-toggle="placeholder"]');
const placeholder = function(group, control, label) {
    addListenerMultiple(control, 'change', function() {
        console.log(control.value);
        if (control.value === '') {
            label.classList.remove('is-hide');
        } else {
            label.classList.add('is-hide');
        }
    });
};
[...placeholders].forEach((label, i) => {
    let group = label.parentNode;
    let control = group.querySelector('.form__control');
    placeholder(group, control, label);
});
