// START custom ajax
const formatParams = function formatParams(params, method) {
    if (typeof params === 'string') {
        return params;
    }

    var letter = method.toLowerCase() === 'post' ? '' : '?';
    if (Array.isArray(params)) {
        return letter + params.map(function (obj) {
            return obj.name + "=" + obj.value;
        }).join("&");
    }
    return letter + Object.keys(params).map(function (key) {
        return key + "=" + params[key];
    }).join("&");
};


const ajax = function ajax(options) {
    const url = options.url,
        method = options.method,
        data = options.data,
        debug = options.debug,
        callback = options.callback,
        error = options.error;

    if (debug) {
        callback('test');
        return;
    }

    let async = options.async === false ? false : true;
    let xhr = new XMLHttpRequest();
    let params = formatParams(data, 'get');
    let body = null;

    if (method.toLowerCase() === 'post') {
        body = formatParams(data, 'post');
        params = '';
    }

    xhr.open(method, url + params, async);
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    xhr.onreadystatechange = function () {
        if (this.readyState === 4) {
            if (this.status === 200) {
                callback(this.responseText);
            } else {
                error && error(this.responseText);
            }
        }
    };
    xhr.send(body);
};
// END custom ajax


export {ajax};
