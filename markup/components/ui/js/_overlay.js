'use strict';

window.overlay = [];

window.overlay.close = function () {
    const page = document.querySelector('.page');
    page.classList.remove('is-overhide');
    const overlays = document.querySelectorAll('.overlay.is-open');
    [].forEach.call(overlays, function (element) {
        element.classList.remove('is-open');
        setTimeout( function () {
            element.style.display = 'none';
        }, 300);
    });
};

window.overlay.show = function (target) {
    const page = document.querySelector('.page');
    let overlay = document.querySelector('.overlay');

    if (!overlay){
        overlay = document.createElement('div');
        page.appendChild(overlay);
        overlay.outerHTML = '<div class="overlay is-open"></div>';
    }

    page.classList.add('is-overhide');
    overlay.style.display = 'block';
    setTimeout( function () {
        overlay.classList.add('is-open');
    }, 300);

};

document.addEventListener('click', function (e) {
    const element = e.target;

    if ( element.classList.contains === 'overlay') {
        window.overlay.close();
        window.modal.close();
    }

});
