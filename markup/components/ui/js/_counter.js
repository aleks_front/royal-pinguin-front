import {addListenerMultiple, closest, forEach} from './_util';
// START counter
function counterPlus(counter) {
    const val = parseFloat(counter.input.value);
    let newVal;
    if (val >= counter.max) {
        newVal = val;
    } else {
        newVal = val + 1;
    }

    // update value
    counterUpdate(counter, newVal);

    // update btn state 
    counterBtnState(counter, newVal);
}

function counterMinus(counter) {
    const val = parseFloat(counter.input.value);
    let newVal;
    if (val <= counter.min) {
        newVal = val;
    } else {
        newVal = val - 1;
    }

    // update value
    counterUpdate(counter, newVal);

    // update btn state 
    counterBtnState(counter, newVal);
}

function counterUpdate(counter, val) {
    counter.input.value = val;
}

function counterBtnState(counter, val) {
    if (val == counter.min) {
        counter.minus.disabled = true;
    } else if (val == counter.max) {
        counter.plus.disabled = true;
    } else {
        counter.minus.disabled = false;
        counter.plus.disabled = false;
    }
}

document.addEventListener('click', function (e) {
    const element = e.target;
    const toggle = element.getAttribute('data-toggle');
    const target = element.getAttribute('data-target');

    if (toggle === 'count') {
        e.preventDefault();
        const counter = [];
        counter.wrap = closest(element, '.form__group');
        counter.input = counter.wrap.querySelector('.form__control');
        counter.minus = counter.wrap.querySelector('[data-target="minus"]');
        counter.plus = counter.wrap.querySelector('[data-target="plus"]');
        counter.min = counter.input.getAttribute('min') ? counter.input.getAttribute('min') : 1;
        counter.max = counter.input.getAttribute('max') ? counter.input.getAttribute('max') : 999;
        counter.step = counter.input.getAttribute('step') ? counter.input.getAttribute('step') : 1;
        if (target === 'plus') {
            counterPlus(counter);
        } else if (target === 'minus') {
            counterMinus(counter);
        }
        // create event counter change
        let event;
        if (window.CustomEvent) {
            event = new CustomEvent('counterChange', {
                detail: {
                    key1: 'data'
                }
            });
        } else {
            event = document.createEvent('CustomEvent');
            event.initCustomEvent('counterChange', true, true, {
                key1: 'data'
            });
        }
        // add event counter change
        counter.input.dispatchEvent(event);
    }
});

let counters = document.querySelectorAll('.form__control--counter');
[...counters].forEach((curentCounter) => {
    curentCounter.addEventListener('counterChange', function(e){
        let product = closest(curentCounter, '.product');
        let counters = product.querySelectorAll('.form__control--counter');
        [...counters].forEach((counter) => {
            if( curentCounter !== counter ) {
                counter.value = curentCounter.value;
            }
        });
    });
});
// END counter
