'use strict';
document.addEventListener('click', (e) => {
    let el = e.target;
    let toggle = el.getAttribute('data-toggle') === 'tabs' ? el : false ;
    if (toggle) {
        let target = document.querySelector(el.getAttribute('data-target')) ? document.querySelector(el.getAttribute('data-target')) : false;
        let parent = document.querySelector(el.getAttribute('data-parent')) ? document.querySelector(el.getAttribute('data-parent')) : false;
        let select = parent.querySelector('[data-toggle="select"]') ? parent.querySelector('[data-toggle="select"]') : false;
        e.preventDefault();
        let children = parent.querySelectorAll('[data-toggle="tabs"]');
        [...children].forEach((childToggle) => {
            let childTarget = document.querySelector(childToggle.getAttribute('data-target'));
            if (childToggle !== el) {
                hide(childToggle, childTarget);
            } else {
                show(childToggle, childTarget);
                if (select) {
                    select.value = childToggle.getAttribute('data-target');
                    console.log(select.value, childToggle.getAttribute('data-target'));
                }
            }
        });
    }
});

let select = document.querySelector('[data-toggle="select"]');
if (select) {
    select.addEventListener('change', (e) => {
        let el = e.target;
        let target = el.value;
        let parent = document.querySelector(el.getAttribute('data-parent')) ? document.querySelector(el.getAttribute('data-parent')) : false;
        if (target && parent) {
            let children = parent.querySelectorAll('[data-toggle="tabs"]');
            [...children].forEach((childToggle) => {
                let childTarget = document.querySelector(childToggle.getAttribute('data-target'));
                if (childToggle.getAttribute('data-target') !== target) {
                    hide(childToggle, childTarget);
                } else {
                    show(childToggle, childTarget);
                }
            });
        }
    });
}

const show = function (toggle, target) {
    let id = toggle.getAttribute('data-target');
    let controls = document.querySelectorAll('[data-target="' + id + '"]');
    [...controls].forEach((element) => {
        let toggle = element.getAttribute('data-toggle');
        if (toggle === 'collapse') {
            element.classList.add('is-active');
        } else if (toggle === 'tabs') {
            element.parentNode.classList.add('is-active');
        }
    });
    target.style.removeProperty('height', 'visibility');
    target.style.display = 'block';
    ///toggle.parentNode.classList.add('is-active');
    target.classList.add('is-open');

    // create event counter change
    let event;
    if (window.CustomEvent) {
        event = new CustomEvent('tabs.show', {
            detail: {
                key1: 'data'
            }
        });
    } else {
        event = document.createEvent('tabs.show');
        event.initCustomEvent('tabs.show', true, true, {
            key1: 'data'
        });
    }
    target.dispatchEvent(event);
}

const hide = function (toggle, target) {
    let id = toggle.getAttribute('data-target');
    let controls = document.querySelectorAll('[data-target="' + id + '"]');
    [...controls].forEach((element) => {
        let toggle = element.getAttribute('data-toggle');
        if (toggle === 'collapse') {
            element.classList.remove('is-active');
        } else if (toggle === 'tabs') {
            element.parentNode.classList.remove('is-active');
        }
    });
    target.style.removeProperty('height', 'visibility');
    target.classList.remove('is-open');
    // toggle.parentNode.classList.remove('is-active');
    target.style.removeProperty('height');
    target.style.display = 'none';
};


