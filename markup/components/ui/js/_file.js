'use strict';
import {addListenerMultiple, forEach, closest} from './_util';

const controls = document.querySelectorAll('[data-toggle="file"]');
if (controls) {
    [...controls].forEach((element, index) => {
        let object = [];
        object.parent = closest(element, '.form__group');
        object.control = object.parent.querySelector('.form__control');
        object.file = object.control.files[0] ? object.control.files[0] : false;
        object.name = object.file.name ? object.file.name : false;

        if (object.parent.querySelector('.form__file')) {
            object.text = object.parent.querySelector('.form__file');
        } else {
            object.parent.insertAdjacentHTML('beforeend', '<div class="form__file" style="display: none;"></div>');
            object.text = object.parent.querySelector('.form__file');
        }

        object.control.addEventListener('change', function (e) {
            object.file = object.control.files[0] ? object.control.files[0] : false;
            object.name = object.file.name ? object.file.name : false;
            if (object.parent.querySelector('.form__file') && object.name) {
                object.text = object.parent.querySelector('.form__file');
            } else if(object.name) {
                object.parent.insertAdjacentHTML('beforeend', '<div class="form__file" style="display: none;"></div>');
                object.text = object.parent.querySelector('.form__file');
            }
            if (object.name) {
                object.text.innerHTML = object.name;
                object.text.style.display = 'block';
            } else {
                object.text.innerHTML = '';
                object.text.style.display = 'none';
            }
        });

        // object.text.addEventListener('click', function (e) {
        //     object.text.innerHTML = '';
        //     object.text.style.display = 'block';
        // });

    });
}
