'use strict';

document.addEventListener('click', function (e) {
    let el = e.target;
    let toggle = el.getAttribute('data-toggle') === 'collapse' ? el : false ;
    let target = document.querySelector(el.getAttribute('data-target')) ? document.querySelector(el.getAttribute('data-target')) : false;
    let parent = document.querySelector(el.getAttribute('data-parent')) ? document.querySelector(el.getAttribute('data-parent')) : false;
    if (target && toggle) {
        if (parent){
            let children = parent.querySelectorAll('[data-toggle="collapse"]');
            [...children].forEach((childToggle, index) => {
                if (childToggle !== toggle) {
                    let childTarget = document.querySelector(childToggle.getAttribute('data-target'));
                    hide(childToggle, childTarget);
                } else {
                    if (target.classList.contains('is-open')) {
                        hide(toggle, target);
                    } else {
                        show(toggle, target);
                    }
                }
            });
        } else {
            if (target.classList.contains('is-open')) {
                hide(toggle, target);
            } else {
                show(toggle, target);
            }
        }
    }
    
});

const show = function (toggle, target) {
    let id = toggle.getAttribute('data-target');
    let controls = document.querySelectorAll('[data-target="' + id + '"]');
    [...controls].forEach((element) => {
        let toggle = element.getAttribute('data-toggle');
        if (toggle === 'collapse') {
            element.classList.add('is-active');
        } else if (toggle === 'tabs') {
            element.parentNode.classList.add('is-active');
        }
    });
    // toggle.classList.add('is-active');
    target.classList.add('is-open');
    target.style.visibility = 'visible';
    target.style.height = target.scrollHeight + 'px';
    setTimeout(() => {
        target.style.removeProperty('height');
        target.style.removeProperty('visibility');
    }, 350);
}

const hide = function (toggle, target) {
    let id = toggle.getAttribute('data-target');
    let controls = document.querySelectorAll('[data-target="' + id + '"]');
    [...controls].forEach((element) => {
        let toggle = element.getAttribute('data-toggle');
        if (toggle === 'collapse') {
            element.classList.remove('is-active');
        } else if (toggle === 'tabs') {
            element.parentNode.classList.remove('is-active');
        }
    });
    // toggle.classList.remove('is-active');
    target.classList.remove('is-open'); 
    target.style.visibility = 'hidden';
    target.style.height = '0px';
    setTimeout(() => {
        target.style.removeProperty('height');
        target.style.removeProperty('visibility');
    }, 350);
};


