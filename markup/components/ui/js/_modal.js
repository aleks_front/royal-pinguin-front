'use strict';

window.modal = [];

window.modal.close = function () {
    const page = document.querySelector('.page');
    page.classList.remove('is-overhide');
    window.overlay.close();
    const modals = document.querySelectorAll('.modal.is-open');
    [].forEach.call(modals, function (element) {
        element.classList.remove('is-open');
        setTimeout( function () {
            element.style.display = 'none';
        }, 300);
    });
};

window.modal.show = function (target) {
    const page = document.querySelector('.page');

    if ( target.length && page ) {
        const modal = document.querySelector(target);
        if ( modal ) {
            page.classList.add('is-overhide');
            modal.style.display = 'block';
            window.overlay.show();
            setTimeout( function () {
                modal.classList.add('is-open');
            }, 300);
        }
    }
};

document.addEventListener('click', function (e) {
    const element = e.target;
    const toggle = element.getAttribute('data-toggle');
    const target = element.getAttribute('data-target');
    const dismis = element.getAttribute('data-dismis');
    if ( toggle === 'modal' && target ) {
        e.preventDefault();
        window.modal.close(); // class all modal
        window.modal.show(target); // open modal by target
    }

    if ( dismis === 'modal') {
        e.preventDefault();
        window.modal.close(); // class all modal
        window.overlay.close(); // class all modal
    }

});
