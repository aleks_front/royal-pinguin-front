'use strict';
import {addListenerMultiple, breakpoints, getOffset} from './_util';
import Popper from 'popper.js';

let smart = document.querySelectorAll('.navSmartJs');
let container = document.querySelector('.navSmartJsContainer');

let resizeTimer;
const resize = function (data) {
    clearTimeout(resizeTimer);
    resizeTimer = setTimeout(function () {
        if (getOffset(document.documentElement).width >= breakpoints.lg) {
            data.popper = new Popper(data.referenceElement, data.onPoper, {
                placement: 'bottom-start',
                modifiers: {
                    flip: {
                        behavior: ['bottom-start', 'bottom', 'bottom-end'],
                        fn(data) {
                            data.offsets.popper.top = data.offsets.reference.height;
                            return data;
                        }
                    },
                    preventOverflow: {
                        enabled: true,
                        boundariesElement: data.container
                    }
                }
            });
        } else if (data.popper) {
            data.popper.destroy();
        }
    }, 250);
};

if (smart && container) {
    [...smart].forEach((element, index) => {
        let object = [];
        object.popper = '';
        object.referenceElement = element.parentNode; // parent nav item for dropdown smart menu
        object.onPoper = element;
        object.container = container;

        addListenerMultiple(window, 'load resize', function () {
            resizeTimer = '';
            resize(object);
        });
    });
}
