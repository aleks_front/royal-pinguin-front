'use strcit';
import {isHidden} from './_util';

const validOptions = {
    colorWrong: '',
    rules: {
        name: {
            required: true,
        },
        email: {
            required: true,
            email: true
        },
        phone: {
            required: true,
        }
    },
    messages: {
        name: {
            required: 'Заполните поле'
        },
        phone: {
            required: 'Заполните поле'
        },
        email: {
            required: 'Заполните поле'
        }
    }
};

let form = document.querySelector('.js-form');
if (form) {
    new window.JustValidate('.js-form', validOptions);
}


// START add phone
document.addEventListener('click', (e) => {
    let el = e.target;
    let toggle = el.getAttribute('data-toggle') === 'control' ? el : false;
    let target = document.querySelector(el.getAttribute('data-target')) ? document.querySelector(el.getAttribute('data-target')) : false;
    if (toggle && target) {
        if (isHidden(target)) {
            target.style.display = 'block';
            let text = toggle.innerText || toggle.textContent;
            toggle.innerHTML = text.replace('ДОБАВИТЬ', 'СКРЫТЬ');
        } else {
            target.style.display = 'none';
            let text = toggle.innerText || toggle.textContent;
            toggle.innerHTML = text.replace('СКРЫТЬ', 'ДОБАВИТЬ');
        }
    }
});
// END add phone
