// Go back by history window
document.addEventListener('click', function (e) {

    const elem = e.target;
    const elemToggle = elem.dataset['toggle'];
    const elemTarget = elem.dataset['target'] === -1 ? true : false;

    if ( elemToggle === 'history' ) {
        e.preventDefault();
        if (elemTarget) {
            history.back();
        } else {
            history.go(elem.dataset['target']);
        }
    }

});
