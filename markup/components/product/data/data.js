var data = {
    product: {
        card: [
            {
                title: 'РАЗВИВАЮЩИЙ КОВРИК "ЛЕСНОЕ ОЗЕРО"',
                img: {
                    x1: 'card__img-3.jpg',
                    x2: 'card__img-3@2x.jpg'
                },
                price: '5 950 руб.',
                loading: true
            },
            {
                title: 'РАЗВИВАЮЩАЯ ИГРУШКА INFANTINO "ЗЕРКАЛЬЦЕ"',
                img: {
                    x1: 'card__img-4.jpg',
                    x2: 'card__img-4@2x.jpg'
                },
                price: '890 руб.'
            },
            {
                title: 'СТУЛЬЧИК ДЛЯ КOPМЛEНИЯ PRIMA PAPPA DINER BEST,PALOMA',
                img: {
                    x1: 'card__img-5.jpg',
                    x2: 'card__img-5@2x.jpg'
                },
                price: '15 920 руб.'
            },
            {
                title: 'ДЕТСКАЯ КРОВАТКА-КАЧАЛКА BABY ITALIA MIMI, ЦВЕТ AVORIO LACCATO',
                img: {
                    x1: 'card__img-6.jpg',
                    x2: 'card__img-6@2x.jpg'
                },
                price: '23 940 руб.'
            },
            {
                title: 'РАЗВИВАЮЩИЙ КОВРИК "ЛЕСНОЕ ОЗЕРО"',
                img: {
                    x1: 'card__img-7.jpg',
                    x2: 'card__img-7@2x.jpg'
                },
                label: 'new',
                price: '5 950 руб.'
            },
            {
                title: 'РАЗВИВАЮЩАЯ ИГРУШКА INFANTINO "ЗЕРКАЛЬЦЕ"',
                img: {
                    x1: 'card__img-8.jpg',
                    x2: 'card__img-8@2x.jpg'
                },
                label: 'sale',
                priceOld: '3000 руб.',
                price: '890 руб.'
            },
            {
                title: 'СТУЛЬЧИК ДЛЯ КOPМЛEНИЯ PRIMA PAPPA DINER BEST,PALOMA',
                img: {
                    x1: 'card__img-9.jpg',
                    x2: 'card__img-9@2x.jpg'
                },
                price: '15 920 руб.'
            },
            {
                title: 'ДЕТСКАЯ КРОВАТКА-КАЧАЛКА BABY ITALIA MIMI, ЦВЕТ AVORIO LACCATO',
                img: {
                    x1: 'card__img-10.jpg',
                    x2: 'card__img-10@2x.jpg'
                },
                price: '23 940 руб.'
            },
            {
                title: 'РАЗВИВАЮЩИЙ КОВРИК "ЛЕСНОЕ ОЗЕРО"',
                img: {
                    x1: 'card__img-11.jpg',
                    x2: 'card__img-11@2x.jpg'
                },
                price: '5 950 руб.'
            },
            {
                title: 'РАЗВИВАЮЩАЯ ИГРУШКА INFANTINO "ЗЕРКАЛЬЦЕ"',
                img: {
                    x1: 'card__img-12.jpg',
                    x2: 'card__img-12@2x.jpg'
                },
                price: '890 руб.'
            },
            {
                title: 'СТУЛЬЧИК ДЛЯ КOPМЛEНИЯ PRIMA PAPPA DINER BEST,PALOMA',
                img: {
                    x1: 'card__img-13.jpg',
                    x2: 'card__img-13@2x.jpg'
                },
                price: '15 920 руб.'
            },
            {
                title: 'ДЕТСКАЯ КРОВАТКА-КАЧАЛКА BABY ITALIA MIMI, ЦВЕТ AVORIO LACCATO',
                img: {
                    x1: 'card__img-14.jpg',
                    x2: 'card__img-14@2x.jpg'
                },
                price: '23 940 руб.'
            },
            {
                title: 'РАЗВИВАЮЩИЙ КОВРИК "ЛЕСНОЕ ОЗЕРО"',
                img: {
                    x1: 'card__img-3.jpg',
                    x2: 'card__img-3@2x.jpg'
                },
                price: '5 950 руб.'
            },
            {
                title: 'РАЗВИВАЮЩАЯ ИГРУШКА INFANTINO "ЗЕРКАЛЬЦЕ"',
                img: {
                    x1: 'card__img-4.jpg',
                    x2: 'card__img-4@2x.jpg'
                },
                price: '890 руб.'
            },
            {
                title: 'СТУЛЬЧИК ДЛЯ КOPМЛEНИЯ PRIMA PAPPA DINER BEST,PALOMA',
                img: {
                    x1: 'card__img-5.jpg',
                    x2: 'card__img-5@2x.jpg'
                },
                price: '15 920 руб.'
            },
            {
                title: 'ДЕТСКАЯ КРОВАТКА-КАЧАЛКА BABY ITALIA MIMI, ЦВЕТ AVORIO LACCATO',
                img: {
                    x1: 'card__img-6.jpg',
                    x2: 'card__img-6@2x.jpg'
                },
                price: '23 940 руб.'
            },
            {
                title: 'РАЗВИВАЮЩИЙ КОВРИК "ЛЕСНОЕ ОЗЕРО"',
                img: {
                    x1: 'card__img-7.jpg',
                    x2: 'card__img-7@2x.jpg'
                },
                label: 'new',
                price: '5 950 руб.'
            },
            {
                title: 'РАЗВИВАЮЩАЯ ИГРУШКА INFANTINO "ЗЕРКАЛЬЦЕ"',
                img: {
                    x1: 'card__img-8.jpg',
                    x2: 'card__img-8@2x.jpg'
                },
                label: 'sale',
                priceOld: '3000 руб.',
                price: '890 руб.'
            },
            {
                title: 'СТУЛЬЧИК ДЛЯ КOPМЛEНИЯ PRIMA PAPPA DINER BEST,PALOMA',
                img: {
                    x1: 'card__img-9.jpg',
                    x2: 'card__img-9@2x.jpg'
                },
                price: '15 920 руб.'
            },
            {
                title: 'ДЕТСКАЯ КРОВАТКА-КАЧАЛКА BABY ITALIA MIMI, ЦВЕТ AVORIO LACCATO',
                img: {
                    x1: 'card__img-10.jpg',
                    x2: 'card__img-10@2x.jpg'
                },
                price: '23 940 руб.'
            },
            {
                title: 'РАЗВИВАЮЩИЙ КОВРИК "ЛЕСНОЕ ОЗЕРО"',
                img: {
                    x1: 'card__img-11.jpg',
                    x2: 'card__img-11@2x.jpg'
                },
                price: '5 950 руб.'
            },
            {
                title: 'РАЗВИВАЮЩАЯ ИГРУШКА INFANTINO "ЗЕРКАЛЬЦЕ"',
                img: {
                    x1: 'card__img-12.jpg',
                    x2: 'card__img-12@2x.jpg'
                },
                price: '890 руб.'
            },
            {
                title: 'СТУЛЬЧИК ДЛЯ КOPМЛEНИЯ PRIMA PAPPA DINER BEST,PALOMA',
                img: {
                    x1: 'card__img-13.jpg',
                    x2: 'card__img-13@2x.jpg'
                },
                price: '15 920 руб.'
            },
            {
                title: 'ДЕТСКАЯ КРОВАТКА-КАЧАЛКА BABY ITALIA MIMI, ЦВЕТ AVORIO LACCATO',
                img: {
                    x1: 'card__img-14.jpg',
                    x2: 'card__img-14@2x.jpg'
                },
                price: '23 940 руб.'
            }
        ]
    }
};
