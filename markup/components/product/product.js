
import {addListenerMultiple, breakpoints, getOffset, forEach} from '../ui/js/_util';
import {ajax} from '../ui/js/_ajax';
import Swiper from 'swiper';

const appendSlide = (slider) => {
    /* global ajaxurl */
    if (ajaxurl) {
        ajax({
            url: ajaxurl,
            method: 'POST',
            data: {
                action: slider.action,
                count: slider.perview * slider.row,
                meta: slider.meta,
                paged: slider.page
            },
            async: true,
            callback: (data) => {
                if (data.length) {
                    console.log(data, JSON.parse(data));
                    slider.object.appendSlide(JSON.parse(data));
                }
            },
            error: (data) => {
                console.log(data);
            }
        });
    }
};

const init = function (data) {
    data.block.classList.add('product--grid-slider');
    data.block.classList.remove('product--grid-offers');
    data.container.classList.add('swiper-container');
    data.wrapper.classList.add('swiper-wrapper');
    if (data.collapse) {
        data.collapse.removeAttribute('data-toggle');
    }
    [...data.slide].forEach((element, index) => {
        element.classList.add('swiper-slide');
    });
    data.object = new Swiper(data.container, {
        slidesPerView: data.perview,
        slidesPerColumn: data.row,
        spaceBetween: 30,
        slidesPerGroup: data.perview,
        navigation: {
            nextEl: data.btnNext,
            prevEl: data.btnPrev
        },
        pagination: {
            el: data.fraction,
            type: 'custom',
            renderCustom: function (swiper, current, total) {
                total = (data.total - data.total % (data.perview * data.row)) / (data.perview * data.row);
                data.page = current + 2;
                return current + '/' + total;
            }
        },
        breakpoints: {
            992: {
                slidesPerView: 4,
                spaceBetween: 12
            }
        },
        on: {
            slideChange: function () {
                if (this.previousIndex < this.activeIndex && data.action) {
                    appendSlide(data);
                }
            }
        }
    });
};

const destroy = function (data) {
    data.object.destroy(true, true);
    data.block.classList.add('product--grid-offers');
    data.block.classList.remove('product--grid-slider');
    data.container.classList.remove('swiper-container');
    data.wrapper.classList.remove('swiper-wrapper');
    if (!data.collapse) {
        data.collapse.setAttribute('data-toggle', 'collapse');
    }
    [...data.slide].forEach((element) => {
        element.classList.remove('swiper-slide');
    });
};

let resizeTimer;
const resize = function (data) {
    clearTimeout(resizeTimer);
    resizeTimer = setTimeout(function () {
        if (getOffset(document.documentElement).width >= breakpoints.md) {
            init(data);
        } else if (data.object && data.object.initialized) {
            destroy(data);
        }
    }, 250);
};

let product = document.querySelectorAll('.productGridOffersJs');

if (product) {
    [...product].forEach((element) => {
        let slider = [];
        slider.object = '';
        slider.block = element;
        slider.collapse = slider.block.querySelector('[data-toggle="collapse"]') ? slider.block.querySelector('[data-toggle="collapse"]') : false;
        slider.container = element.querySelector('.product__container');
        slider.wrapper = element.querySelector('.product__row');
        slider.slide = element.querySelectorAll('.product__col');
        slider.btnPrev = element.querySelector('.product__btn--prev');
        slider.btnNext = element.querySelector('.product__btn--next');
        slider.fraction = element.querySelector('.product__fraction');
        slider.perview = element.getAttribute('data-perview') ? element.getAttribute('data-perview') : 4;
        slider.total = element.getAttribute('data-total') ? element.getAttribute('data-total') : false;
        slider.row = element.getAttribute('data-row') ? element.getAttribute('data-row') : 1;
        slider.action = element.getAttribute('data-action') ? element.getAttribute('data-action') : false;
        slider.meta = element.getAttribute('data-meta') ? element.getAttribute('data-meta') : false;

        addListenerMultiple(window, 'load resize', function () {
            resizeTimer = '';
            resize(slider);
        });
    });
}
