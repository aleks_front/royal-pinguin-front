
import {addListenerMultiple, breakpoints, getOffset, forEach} from '../ui/js/_util';
import Swiper from 'swiper';

const sliderProductMainJs = document.querySelector('.sliderProductMainJs');
const sliderProductSecondaryJs = document.querySelector('.sliderProductSecondaryJs');

if (sliderProductMainJs) {
    const primary = [];
    primary.init = true;
    primary.slider = sliderProductMainJs;
    primary.container = primary.slider.querySelector('.slider__container');
    primary.pagination = primary.slider.querySelector('.slider__pagination');
    primary.object = new Swiper(primary.container, {
        slidesPerView: 1,
        zoom: true,
        lazy: {
            elementClass: 'slider__img',
            loadingClass: 'is-loading',
            loadedClass: 'is-loaded',
            loadPrevNext: true,
            preloaderClass: 'slider__loader'
        },
        pagination: {
            el: primary.pagination,
            clickable: true,
            bulletClass: 'slider__pagination-item',
            bulletActiveClass: 'is-active'
        }
    });
    const secondary = [];
    if (sliderProductSecondaryJs) {
        secondary.init = true;
        secondary.slider = sliderProductSecondaryJs;
        secondary.container = secondary.slider.querySelector('.slider__container');
        secondary.next = secondary.slider.querySelector('.slider__btn--next');
        secondary.prev = secondary.slider.querySelector('.slider__btn--prev');
        secondary.object = new Swiper(secondary.container, {
            spaceBetween: 10,
            slidesPerView: 3,
            touchRatio: 0.2,
            slideToClickedSlide: false,
            // centeredSlides: false,
            direction: 'vertical',
            navigation: {
                nextEl: secondary.next,
                disabledClass: 'is-disabled'
            },
            on: {
                click: function (event) {
                    if (typeof secondary.object.clickedIndex !== 'undefined') {
                        primary.object.slideTo(secondary.object.clickedIndex);
                    }
                }
            }
        });
        primary.object.controller.control = secondary.object;
        secondary.object.controller.control = primary.object;
    }
}


// START product description
let description = document.querySelector('#productTabs');
if (description) {
    let content = description.querySelectorAll('.product__tab-body');
    let resizeTimer;
    const resize = function () {
        clearTimeout(resizeTimer);
        resizeTimer = setTimeout(function () {
            let windowWidth = getOffset(document.documentElement).width;
            [...content].forEach((element, index) => {
                if (windowWidth >= breakpoints.sm) {
                    element.style.removeProperty('height');
                    element.style.removeProperty('visibility');
                } else {
                    element.style.removeProperty('display');
                }
            });
        }, 250);
    };

    addListenerMultiple(window, 'load resize', function () {
        resizeTimer = '';
        resize();
    });
}
// END product description
