
import {closest} from '../ui/js/_util';
import {ajax} from '../ui/js/_ajax';
const userEdit = document.querySelectorAll('.user__info-link');

const show = function (target) {
    target.classList.add('is-open');
    target.style.visibility = 'visible';
    target.style.height = target.scrollHeight + 'px';
    setTimeout(() => {
        target.style.removeProperty('height');
        target.style.removeProperty('visibility');
    }, 350);
};

const hide = function (target) {
    // toggle.classList.remove('is-active');
    target.classList.remove('is-open');
    target.style.visibility = 'hidden';
    target.style.height = '0px';
    setTimeout(() => {
        target.style.removeProperty('height');
        target.style.removeProperty('visibility');
    }, 350);
};

const edit = (data) => {
    /* global ajaxurl */
    if (ajaxurl) {
        ajax({
            url: ajaxurl,
            method: 'POST',
            data: {
                action: data.action,
                id: data.id
            },
            async: true,
            callback: (resp) => {
                let response = JSON.parse(resp);
                console.log(response);
                if (response.success) {
                    let form = document.querySelector(data.target);
                    show(form);
                    for (let name in response) {
                        if (response.hasOwnProperty(name)) {
                            if (form.querySelectorAll('[name="' + CSS.escape(name) + '"]')) {
                                let control = form.querySelectorAll('[name="' + CSS.escape(name) + '"]');
                                [...control].forEach((element, index) => {
                                    if (element.getAttribute('type') === 'checkbox' || element.getAttribute('type') === 'radio') {
                                        if (element.value === response[name]) {
                                            element.checked = true;
                                        }
                                    } else {
                                        element.value = response[name];
                                    }
                                });
                            }
                        }
                    }
                }
            },
            error: (resp) => {
                let form = document.querySelector(data.target);
                hide(form);
            }
        });
    }
};


const removeRow = (data) => {
    let form = document.querySelector(data.target);
    form.reset();
    hide(form);
    /* global ajaxurl */
    if (ajaxurl) {
        ajax({
            url: ajaxurl,
            method: 'POST',
            data: {
                action: data.action,
                id: data.id
            },
            async: true,
            callback: (resp) => {
                let response = JSON.parse(resp);
                console.log(response);
                if (response.success) {
                    let userInfo = closest(data.el, '.user__info');
                    userInfo.remove();
                }
            },
            error: (resp) => {
                console.log('error' + JSON.parse(resp));
            }
        });
    }
};


[...userEdit].forEach((element) => {
    element.addEventListener('click', (e) => {
        e.preventDefault();
        let data = [];
        data.el = e.target;
        data.toggle = data.el.getAttribute('data-toggle') ? data.el.getAttribute('data-toggle') : false;
        data.target = data.el.getAttribute('data-target') ? data.el.getAttribute('data-target') : false;
        data.action = data.el.getAttribute('data-action') ? data.el.getAttribute('data-action') : false;
        data.id = data.el.getAttribute('data-id') ? data.el.getAttribute('data-id') : false;
        if (data) {
            e.preventDefault();
            if (data.toggle === 'edit') {
                edit(data);
            } else if (data.toggle === 'delete') {
                removeRow(data);
            }
        }
    });
});

